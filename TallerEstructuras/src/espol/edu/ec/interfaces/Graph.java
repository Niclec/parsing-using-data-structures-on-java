/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.interfaces;

import java.util.List;

/**
 *
 * @author 
 * @param <E>
 */
public interface Graph<E> {
    int size();
    boolean isEmpty();
    boolean addVertice(E elemento);
    boolean addArco(E origen, E destino, int peso);
    boolean removeArco(E origen, E destino);
    boolean removeVertice(E elemento);
    boolean contains(E elemento);
    boolean esConexo();
    List<E> recorridoBfs(E elemento);
    List<E> recorridoDfs(E elemento);
    List<List<E>> getComponentesConexas();
    
}
