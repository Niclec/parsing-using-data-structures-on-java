/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.conversion;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author user
 */
public class BuscadorPaths {
    
    private static String path;
    private  BorderPane rootPrincipal;
    private Button activate;
    
    public BuscadorPaths(){
        rootPrincipal=new BorderPane();
        activate = new Button("Buscar CSV");
        this.rootPrincipal.setCenter(activate);
        this.cargarArchivo();
    }
    
    private void cargarArchivo(){
        activate.setOnAction(e->{
            pathFinder();
        });
    }
    
    public void pathFinder(){
        
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.dir"))
        );                 
        fileChooser.getExtensionFilters().addAll(
            //new FileChooser.ExtensionFilter("All Images", "*.*"),
            new FileChooser.ExtensionFilter("TXT", "*.txt"),
            new FileChooser.ExtensionFilter("CSV", "*.csv")
            );
        
        List<File> list = (List<File>) fileChooser.showOpenMultipleDialog((Stage) this.rootPrincipal.getScene().getWindow());
                    if (list != null) {
                        list.forEach((file) -> {
                            saveFile(file);
            });
        }
    }
    
        public void saveFile(File file){
        String destPath = System.getProperty("user.dir");
        destPath += "/src/";
        File file2 = new File(destPath);
        BuscadorPaths.path=file.getAbsolutePath();
        try {
            FileUtils.copyFileToDirectory(file, file2);           
            
        } catch (IOException ex) {
            Logger.getLogger(BuscadorPaths.class.getName()).log(Level.SEVERE, null, ex);
            
        }
                
 
    }

    public static String getPath() {
        return path;
    }

    public static void setPath(String path) {
        BuscadorPaths.path = path;
    }

        
        
    public BorderPane getRootPrincipal() {
        return rootPrincipal;
    }

    public void setRootPrincipal(BorderPane rootPrincipal) {
        this.rootPrincipal = rootPrincipal;
    }
    
    
    
}
