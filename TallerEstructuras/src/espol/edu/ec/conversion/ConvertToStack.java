/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.conversion;

import espol.edu.ec.tdas.LinkedStack;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ConvertToStack {
    
    private ConvertToStack(){}
    
    public static void convertToStack(LinkedStack stack) throws IOException{
        
        
        try (BufferedReader br= new BufferedReader(new FileReader(BuscadorPaths.getPath()));){
            String linea="";
            while((linea=br.readLine())!=null){
                String[] datos= linea.split(",");
                
                stack.push(ModifyCSVtoObjects.convertToMano(datos));
                
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BuscadorPaths.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    
}
