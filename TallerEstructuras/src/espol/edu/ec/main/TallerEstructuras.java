/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;

import espol.edu.ec.conversion.BuscadorPaths;
import espol.edu.ec.conversion.ConvertToList;
import espol.edu.ec.tdas.ArrayList;
import java.io.IOException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author user
 */
public class TallerEstructuras extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        BuscadorPaths buscador = new BuscadorPaths();
        Scene scene = new Scene(buscador.getRootPrincipal());
        scene.getStylesheets().add(getClass().getResource("mystyle.css").toExternalForm());
        primaryStage.setTitle("Subir archivo");
        primaryStage.setScene(scene);
        primaryStage.setResizable(true);
        primaryStage.show();
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        launch(args);
        ArrayList array = new ArrayList();
        //SAMPLE OF CSV'S PARSING
        ConvertToList.ConvertToArrayList(array);
        System.out.println(array.get(0));

    }

    
}
