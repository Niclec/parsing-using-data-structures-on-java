/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.pokerhand;

import espol.edu.ec.interfaces.List;
import java.util.Objects;

/**
 *
 * @author user
 */
public class Mano {
    String tipo;
    List<Carta> cartaEnMano;
    
    public Mano(String tipo, List<Carta> cartaEnMano){
        this.tipo = tipo;
        this.cartaEnMano = cartaEnMano;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<Carta> getCartaEnMano() {
        return cartaEnMano;
    }

    public void setCartaEnMano(List<Carta> cartaEnMano) {
        this.cartaEnMano = cartaEnMano;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mano other = (Mano) obj;
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mano{" + "tipo=" + tipo + '}';
    }
    
    
    
    
    
}
